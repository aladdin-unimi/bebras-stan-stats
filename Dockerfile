FROM jupyter/datascience-notebook
LABEL maintainer="Mattia Monga <mattia.monga@unimi.it>"
USER root
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -yq dvipng libgeos-c1 r-base && apt-get clean && rm -rf /var/lib/apt/*
USER jovyan
RUN conda install -y -c conda-forge jupyter_contrib_nbextensions
RUN conda install -y -c conda-forge jupyter_nbextensions_configurator
RUN conda install -y -c conda-forge pystan
RUN conda install -y -c r rpy2
USER jovyan
