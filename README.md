This repository contains the code and data needed to replicate the analyses
of [Bebras](http://bebras.org) 2016 described in the article: *How presentation affects the difficulty of computational thinking tasks: an IRT analysis
Paper*	(Koli Calling 2017, November 16–19, 2017, Koli, Finland https://doi.org/10.1145/3141880.3141900)

It contains the following files:

- `*.ipynb` [Jupyter python](https://jupyter.org/) notebooks;
- `*.csv` Anonymous (solvers have a unique hash code) data about the tasks (see
  notebooks);
- `*.stan` [Stan](http://mc-stan.org/) models (see notebooks);
- `Dockerfile` [Docker](https://www.docker.com/) build file to replicate the
  execution environment:
    `docker build -t bebras-stats .`
- `run.sh` script to run the docker image (a browser pointing to the notebooks
  can be started with `run-notebook.sh`)

Results can be read [here](https://aladdin-unimi.gitlab.io/bebras-stan-stats/).
[![build status](https://gitlab.com/aladdin-unimi/bebras-stan-stats/badges/master/build.svg)](https://gitlab.com/aladdin-unimi/bebras-stan-stats/commits/master)
