// 2-PL model: see Stan Reference Manual (6.10 Item-Response Theory Models, p. 67)
data {
  int<lower=1> N_SOLVERS;
  int<lower=1> N_ITEMS;
  int<lower=1> N_OBS;

  int<lower=1, upper=N_SOLVERS> solver[N_OBS];
  int<lower=1, upper=N_ITEMS> item[N_OBS];

  int<lower=0, upper=N_ITEMS> modified[N_ITEMS];

  int<lower=0, upper=1> y[N_OBS]; // result
}

parameters {
  real mean_ability;
  // ability of the solver w.r.t. mean_ability
  vector[N_SOLVERS] ability;
  // difficulty of the item w.r.t mean_ability
  vector[N_ITEMS] difficulty;
  // discrimination is positive: no items easier for lesser ability
  vector<lower=0>[N_ITEMS] discrimination;

  real<lower=0> sigma_difficulty;
  real<lower=0> sigma_discrimination;
}

model { // once in a leapfrog
  ability ~ normal(0, 1);
  difficulty ~ normal(0, sigma_difficulty);
  discrimination ~ lognormal(0, sigma_discrimination);

  // weakly informative priors
  mean_ability ~ cauchy(0, 5);
  sigma_difficulty ~ cauchy(0, 5);
  sigma_discrimination ~ cauchy(0, 5);

  y ~ bernoulli_logit(discrimination[item] .* (ability[solver] - (difficulty[item] + mean_ability)));

}

generated quantities {
  // only for modified items
  vector[N_ITEMS] delta_difficulty;
  vector[N_ITEMS] delta_discrimination;

  for (i in 1:N_ITEMS){
    if (modified[i] == 0){
      delta_difficulty[i] = 0;
      delta_discrimination[i] = 0;
    } else {
      delta_difficulty[i] = difficulty[i] - difficulty[modified[i]];
      delta_discrimination[i] = discrimination[i] - discrimination[modified[i]];
    }
  }
}
