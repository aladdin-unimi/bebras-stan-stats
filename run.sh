#!/bin/bash
docker run -it --rm -p 127.0.0.1:0:8888 \
       -u $(id -u):$(id -g) -v $(pwd):/home/jovyan/work \
       --name bstats mmonga/stanlab start-notebook.sh --NotebookApp.token=''
